﻿using System;
using System.Collections.Generic;
using System.Linq;
using SitecoreCms.CodeSamples.Entities;
using MongoDB.Driver;
using System.Configuration;
using SitecoreCms.CodeSamples.MongoRepository.Documents;
using MongoDB.Driver.Builders;
using MongoDB.Bson;

namespace SitecoreCms.CodeSamples.MongoRepository
{
    public class RocketRepository : IExternalDataRepository
    {
        private const string ConnectionStringName = "rockets";
        private const string DatabaseName = "capture-alpha";
        private const string CollectionName = "Rockets";

        private readonly MongoClient _client;
        private readonly MongoServer _server;
        private readonly MongoDatabase _database;
        private readonly MongoCollection _collection;

        public RocketRepository()
        {
            var connectionString = GetDatabaseConnectionString();

            _client = new MongoClient(connectionString);

            if (_client != null)
            {
                _server = _client.GetServer();

                _database = _server.GetDatabase(DatabaseName);

                _collection = _database.GetCollection<IRocket>(CollectionName);
            }

        }

        private string GetDatabaseConnectionString()
        {
            var connectionStrings = ConfigurationManager.ConnectionStrings;

            if (connectionStrings != null && connectionStrings.Count > 0)
            {
                var connectionSettings = connectionStrings[ConnectionStringName];

                if (connectionSettings != null && !string.IsNullOrWhiteSpace(connectionSettings.ConnectionString))
                    return connectionSettings.ConnectionString;

                var errorMessage = string.Format("{0} requires a connection string named {1} but it could not be found.", this.GetType().Name, ConnectionStringName);

                throw new InvalidOperationException(errorMessage);
            }

            throw new InvalidOperationException("No database connection strings have been found.");
        }

        public IRocket GetById(string rocketId)
        {
            var id = ObjectId.Empty;

            if (ObjectId.TryParse(rocketId, out id))
            {
                var query = Query<Rocket>.EQ(s => s.Id, id);

                return _collection.FindOneAs<Rocket>(query);
            }

            return null;
        }

        public IEnumerable<IRocket> GetByParentId(string parentId)
        {
            var query = Query<Rocket>.EQ(s => s.ParentId, parentId);

            return _collection.FindAs<Rocket>(query);
        }

        public void Save(IRocket rocket)
        {
            var rocketDocument = new Rocket(rocket);

            _collection.Save(rocketDocument);
        }

        public void Delete(IRocket rocket)
        {
            var rocketDocument = new Rocket(rocket);

            var query = Query<Rocket>.EQ(s => s.Id, rocketDocument.Id);

            _collection.Remove(query);
        }

        public void Seed()
        {
            var rocket1 = new Rocket()
                    {
                        ParentId = null,
                        RocketId = "1001",
                        Name = "Falcon 9",
                        Title = "The Falcon 9",
                        Description = "Falcon 9 is a family of launch vehicles designed and manufactured by SpaceX, headquartered in Hawthorne, California."
                    };
            var rocket2 = new Rocket()
                {
                    ParentId = null,
                    RocketId = "1002",
                    Name = "Titan",
                    Title = "The Titan Rocket",
                    Description = "Titan was a family of U.S. expendable rockets used between 1959 and 2005."
                };

            Save(rocket1);
            Save(rocket2);
        }
    }
}

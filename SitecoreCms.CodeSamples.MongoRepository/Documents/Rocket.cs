﻿using System;
using System.Linq;
using SitecoreCms.CodeSamples.Entities;
using MongoDB.Bson;

namespace SitecoreCms.CodeSamples.MongoRepository.Documents
{
    public class Rocket : IRocket
    {
        private ObjectId _id;        

        public ObjectId Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }

        public string RocketId
        {
            get
            {
                return _id.ToString();
            }
            set
            {

                ObjectId.TryParse(value, out _id);
            }
        }

        public string ParentId { get; set; }
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public Rocket()
        {
        }

        public Rocket(IRocket rocket)
        {
            this.ParentId = rocket.ParentId;
            this.Name = rocket.Name;
            this.Title = rocket.Title;
            this.Description = rocket.Description;
        }
    }
}

using System.Collections.Generic;

namespace SitecoreCms.CodeSamples.Entities
{
    public interface IExternalDataRepository
    {
        IRocket GetById(string rocketId);
        IEnumerable<IRocket> GetByParentId(string parentId);
        void Save(IRocket rocket);
        void Delete(IRocket rocket);
        void Seed();
    }
}
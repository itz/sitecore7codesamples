﻿using System;
using System.Linq;

namespace SitecoreCms.CodeSamples.Entities
{
    public interface IRocket 
    {
        string ParentId { get; set; }
        string RocketId { get; set; }        
        string Name { get; set; }
        string Title { get; set; }
        string Description { get; set; }
    }
}

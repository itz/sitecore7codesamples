﻿using SitecoreCms.CodeSamples.Entities;
namespace SitecoreCms.CodeSamples.DataProviders.Entities
{
    public class Rocket : IRocket 
    {
        public string ParentId { get; set; }
        public string RocketId { get; set; }        
        public string Name { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
    }
}

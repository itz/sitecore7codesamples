﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.DataProviders;
using Sitecore.Data.IDTables;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using SitecoreCms.CodeSamples.DataProviders.Entities;

namespace SitecoreCms.CodeSamples.DataProviders.SimpleReadOnlyData
{
    public class RocketReadOnlyDataProvider : DataProvider
    {
        private readonly string _targetDatabaseName;
        private readonly string _idTablePrefix;
        private readonly ID _rocketTemplateID;
        private readonly ID _rocketRootTemplateID;

        private readonly IEnumerable<Rocket> _rockets;

        public RocketReadOnlyDataProvider(string targetDatabaseName, string rocketRootTemplateID, string rocketTemplateID, string idTablePrefix)
        {
            Assert.ArgumentNotNullOrEmpty(targetDatabaseName, "targetDatabaseName");
            Assert.ArgumentNotNullOrEmpty(rocketRootTemplateID, "rootTemplateId");
            Assert.ArgumentNotNullOrEmpty(rocketTemplateID, "simpleReadOnlyDataTemplateId");
            Assert.ArgumentNotNullOrEmpty(idTablePrefix, "idTablePrefix");

            _targetDatabaseName = targetDatabaseName;
            _idTablePrefix = idTablePrefix;

            if (!ID.TryParse(rocketRootTemplateID, out _rocketRootTemplateID))
                throw new InvalidOperationException(string.Format("Invalid rocket root template ID {0}", rocketRootTemplateID));

            if (!ID.TryParse(rocketTemplateID, out _rocketTemplateID))
                throw new InvalidOperationException(string.Format("Invalid rocket template ID {0}", rocketTemplateID));

            _rockets = new RocketRepository().GetSimpleRocketCollection();
        }

        public override ItemDefinition GetItemDefinition(ID itemID, CallContext context)
        {
            Assert.ArgumentNotNull(itemID, "itemID");

            // Retrieve the rocket id from Sitecore's IDTable
            var rocketId = GetRocketIdFromIDTable(itemID);

            if (!string.IsNullOrEmpty(rocketId))
            {
                // Retrieve the rocket data from the rockets collection
                var rocket = _rockets.FirstOrDefault(o => o.RocketId == rocketId);

                if (rocket != null)
                {
                    // Ensure the rocket item name is valid for the Sitecore content tree
                    var itemName = ItemUtil.ProposeValidItemName(rocket.Name);

                    // Return a Sitecore item definition for the rocket using the rocket template
                    return new ItemDefinition(itemID, itemName, ID.Parse(_rocketTemplateID), ID.Null);
                }
            }

            return null;
        }
       
        private string GetRocketIdFromIDTable(ID itemID)
        {
            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemID);

            if (idTableEntries.Any())
                return idTableEntries[0].Key.ToString();

            return null;
        }

        public override ID GetParentID(ItemDefinition itemDefinition, CallContext context)
        {
            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemDefinition.ID);

            if (idTableEntries.Any())
            {
                return idTableEntries.First().ParentID;
            }

            return base.GetParentID(itemDefinition, context);
        }

        public override IDList GetChildIDs(ItemDefinition parentItem, CallContext context)
        {
            if (CanProcessParent(parentItem.ID))
            {
                var itemIdList = new IDList();

                foreach (var rocket in _rockets)
                {
                    var rocketId = rocket.RocketId;

                    // Retrieve the Sitecore item ID mapped to his rocket
                    IDTableEntry mappedID = IDTable.GetID(_idTablePrefix, rocketId);

                    if (mappedID == null)
                    {
                        // Map this rocket to a Sitecore item ID
                        mappedID = IDTable.GetNewID(_idTablePrefix, rocketId, parentItem.ID);
                    }

                    itemIdList.Add(mappedID.ID);
                }

                context.DataManager.Database.Caches.DataCache.Clear();

                return itemIdList;
            }

            return base.GetChildIDs(parentItem, context);
        }

        public override LanguageCollection GetLanguages(CallContext context)
        {
            return null;
        }

        private bool CanProcessParent(ID id)
        {
            var item = Factory.GetDatabase(_targetDatabaseName).Items[id];

            bool canProcess = false;

            if (item.Paths.IsContentItem && item.TemplateID == _rocketRootTemplateID)
            {
                canProcess = true;
            }

            return canProcess;
        }

        public override FieldList GetItemFields(ItemDefinition itemDefinition, VersionUri version, CallContext context)
        {
            var fields = new FieldList();

            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemDefinition.ID);

            if (idTableEntries.Any())
            {
                if (context.DataManager.DataSource.ItemExists(itemDefinition.ID))
                {
                    ReflectionUtil.CallMethod(typeof(ItemCache), CacheManager.GetItemCache(context.DataManager.Database), "RemoveItem", true, true, new object[] { itemDefinition.ID });
                }

                var template = TemplateManager.GetTemplate(_rocketTemplateID, Factory.GetDatabase(_targetDatabaseName));

                if (template != null)
                {
                    var rocketId = GetRocketIdFromIDTable(itemDefinition.ID);

                    if (!string.IsNullOrEmpty(rocketId))
                    {
                        var rocket = _rockets.FirstOrDefault(o => o.RocketId == rocketId);

                        if (rocket != null)
                        {
                            foreach (var field in GetDataFields(template))
                            {
                                fields.Add(field.ID, GetFieldValue(field, rocket));
                            }
                        }
                    }
                }
            }

            return fields;
        }

        protected virtual IEnumerable<TemplateField> GetDataFields(Template template)
        {
            return template.GetFields().Where(ItemUtil.IsDataField);
        }

        private string GetFieldValue(TemplateField field, Rocket rocket)
        {
            string fieldValue = string.Empty;

            switch (field.Name)
            {
                case "Title":
                    fieldValue = rocket.Title;
                    break;
                case "Description":
                    fieldValue = rocket.Description;
                    break;
                default:
                    break;
            }

            return fieldValue;
        }
    }
}

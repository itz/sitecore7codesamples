﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.DataProviders;
using Sitecore.Data.IDTables;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using SitecoreCms.CodeSamples.DataProviders.Entities;

namespace SitecoreCms.CodeSamples.DataProviders.SimpleReadOnlyData
{
    public class SimpleHierachicalReadOnlyDataProvider : DataProvider
    {
        private Database ContentDB
        {
            get
            {
                return Factory.GetDatabase(_targetDatabaseName);
            }
        }

        private readonly string _targetDatabaseName;
        private readonly string _idTablePrefix;
        private readonly ID _simpleReadOnlyDataTemplateID;
        private readonly ID _rootItemID;
        private readonly ID _rootTemplateID;

        private readonly RocketRepository _externalDataRepository;

        public SimpleHierachicalReadOnlyDataProvider(string targetDatabaseName, string rootTemplateId, string rootItemId, string simpleReadOnlyDataTemplateId, string idTablePrefix)
        {
            Assert.ArgumentNotNullOrEmpty(targetDatabaseName, "targetDatabaseName");
            Assert.ArgumentNotNullOrEmpty(rootTemplateId, "rootTemplateId");
            Assert.ArgumentNotNullOrEmpty(rootItemId, "rootItemId");
            Assert.ArgumentNotNullOrEmpty(simpleReadOnlyDataTemplateId, "simpleReadOnlyDataTemplateId");
            Assert.ArgumentNotNullOrEmpty(idTablePrefix, "idTablePrefix");

            _targetDatabaseName = targetDatabaseName;
            _idTablePrefix = idTablePrefix;

            if (!ID.TryParse(rootTemplateId, out _rootTemplateID))
                throw new InvalidOperationException(string.Format("Could not parse to ID using {0}", rootTemplateId));

            if (!ID.TryParse(rootItemId, out _rootItemID))
                throw new InvalidOperationException(string.Format("Could not parse to ID using {0}", rootItemId));

            if (!ID.TryParse(simpleReadOnlyDataTemplateId, out _simpleReadOnlyDataTemplateID))
                throw new InvalidOperationException(string.Format("Could not parse to ID using {0}", simpleReadOnlyDataTemplateId));

            // Get an in memory repository with some sample data
            _externalDataRepository = new RocketRepository();
        }

        public override IDList GetChildIDs(ItemDefinition parentItem, CallContext context)
        {
            var parentItemID = parentItem.ID;
            var parentItemName = parentItem.Name;
            var parentItemTemplateID = parentItem.TemplateID;
            if (CanProcessParent(parentItem.ID))
            {
                // Do not need to drop to any other data providers so abort the context
                context.Abort();

                IEnumerable<Rocket> externalDataCollection = _externalDataRepository.GetSimpleHierarchicalExternalDataCollection();               

                // Get the key for this parent item
                var idTableEntries = IDTable.GetKeys(_idTablePrefix, parentItem.ID);

                IEnumerable<Rocket> filteredexternalDataCollection = null;

                if (parentItem.ID == _rootItemID)
                {
                    filteredexternalDataCollection = externalDataCollection.Where(o => o.ParentId == null);
                }
                else if (idTableEntries.Any())
                {
                    var parentKey = idTableEntries.FirstOrDefault();

                    filteredexternalDataCollection = externalDataCollection.Where(o => o.ParentId == parentKey.Key);
                }               

                // List of child item ids
                var itemIdList = new IDList();

                foreach (var externalData in filteredexternalDataCollection)
                {
                    var externalDataId = externalData.RocketId;

                    IDTableEntry mappedID = IDTable.GetID(_idTablePrefix, externalDataId);

                    if (mappedID == null)
                    {
                        mappedID = IDTable.GetNewID(_idTablePrefix, externalDataId, parentItem.ID);
                    }

                    itemIdList.Add(mappedID.ID);
                }

                // Are you sure you want to do this !
                context.DataManager.Database.Caches.DataCache.Clear();

                return itemIdList;
            }

            return base.GetChildIDs(parentItem, context);
        }

        private bool CanProcessParent(ID id)
        {
            var item = Factory.GetDatabase("master").Items[id];

            bool canProcess = false;

            var validParentItemTemplateIDs = new ID[] { _rootTemplateID, _simpleReadOnlyDataTemplateID };

            // Process when 1 - the item is the data provider root folder
            //              2 - the item is based on the template fis one of the allowed templates
            // item.Paths.IsContentItem && 
            if (validParentItemTemplateIDs.Contains(item.TemplateID))
            {
                canProcess = true;
            }

            return canProcess;
        }
        public override ID GetRootID(CallContext context)
        {
            return _rootItemID;
        }
        public override ItemDefinition GetItemDefinition(ID itemID, CallContext context)
        {
            Assert.ArgumentNotNull(itemID, "itemID");

            if (context.CurrentResult == null)
            {
                var externalDataKey = GetExternalDataKeyFromIDTable(itemID);

                // If no idTable entries then this item is not mapped to any external data
                if (!string.IsNullOrEmpty(externalDataKey))
                {
                    // Get the collection of external data
                    var externalDataCollection = _externalDataRepository.GetSimpleHierarchicalExternalDataCollection();

                    // Get the external data that will appear as an item in the Sitecore content tree
                    var externalData = externalDataCollection.FirstOrDefault(o => o.RocketId == externalDataKey);

                    if (externalData != null)
                    {
                        var itemName = ItemUtil.ProposeValidItemName(externalData.Name);

                        return new ItemDefinition(itemID, itemName, ID.Parse(_simpleReadOnlyDataTemplateID), ID.Null);
                    }
                }
            }

            return null;
        }

        private string GetExternalDataKeyFromIDTable(ID itemID)
        {
            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemID);

            if (idTableEntries != null && idTableEntries.Length > 0)
                return idTableEntries[0].Key.ToString();

            return null;
        }

        public override ID GetParentID(ItemDefinition itemDefinition, CallContext context)
        {
            if (CanProcessItem(itemDefinition.ID))
            {
                context.Abort();

                var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemDefinition.ID);

                if (idTableEntries.Any())
                {
                    return idTableEntries.First().ParentID;
                }
            }

            return base.GetParentID(itemDefinition, context);
        }

        private bool CanProcessItem(ID id)
        {
            return IDTable.GetKeys(_idTablePrefix, id).Length > 0;
        }

        public override FieldList GetItemFields(ItemDefinition item, VersionUri version, CallContext context)
        {
            var fields = new FieldList();

            if (CanProcessChild(item.ID))
            {
                if (context.DataManager.DataSource.ItemExists(item.ID))
                {
                    ReflectionUtil.CallMethod(typeof(ItemCache), CacheManager.GetItemCache(context.DataManager.Database), "RemoveItem", true, true, new object[] { item.ID });
                }

                var template = TemplateManager.GetTemplate(_simpleReadOnlyDataTemplateID, ContentDB);
                if (template != null)
                {
                    var externalDataKey = GetExternalDataKeyFromIDTable(item.ID);

                    // If no idTable entries then this item is not mapped to any external data
                    if (!string.IsNullOrEmpty(externalDataKey))
                    {
                        // Get the collection of external data
                        var externalDataCollection = _externalDataRepository.GetSimpleRocketCollection();

                        // Get the external data that will appear as an item in the Sitecore content tree
                        var externalData = externalDataCollection.FirstOrDefault(o => o.RocketId == externalDataKey);

                        if (externalData != null)
                        {
                            foreach (var field in GetDataFields(template))
                            {
                                fields.Add(field.ID, GetFieldValue(field, externalData));
                            }
                        }
                    }
                }
            }

            return fields;
        }

        private bool CanProcessChild(ID id)
        {
            if (IDTable.GetKeys(_idTablePrefix, id).Length > 0)
            {
                return true;
            }

            return false;
        }

        // Filters template fields to data fields only (excludes fields of a StandardTemplate data template).
        protected virtual IEnumerable<TemplateField> GetDataFields(Template template)
        {
            return template.GetFields().Where(ItemUtil.IsDataField);
        }

        private string GetFieldValue(TemplateField field, Rocket externalData)
        {
            string val = string.Empty;

            switch (field.Name)
            {
                case "Title":
                    val = externalData.Title;
                    break;
                case "Description":
                    val = externalData.Description;
                    break;
                default:
                    break;
            }

            if (val == null)
            {
                val = string.Empty;
            }

            return val;
        }
    }
}

﻿using System.Collections.Generic;
using SitecoreCms.CodeSamples.DataProviders.Entities;

namespace SitecoreCms.CodeSamples.DataProviders.SimpleReadOnlyData
{
    public class RocketRepository
    {
        public IEnumerable<Rocket> GetSimpleRocketCollection()
        {
            var rockets = new List<Rocket>()
            {
                new Rocket()
                    { 
                        ParentId = null, 
                        RocketId = "1001", 
                        Name = "Falcon 9", 
                        Title = "The Falcon 9", 
                        Description = "Falcon 9 is a family of launch vehicles designed and manufactured by SpaceX, headquartered in Hawthorne, California."
                    },
                new Rocket()
                    { 
                        ParentId = null, 
                        RocketId = "1002", 
                        Name = "Titan", 
                        Title = "The Titan Rocket", 
                        Description = "Titan was a family of U.S. expendable rockets used between 1959 and 2005."
                    },                
            };
            return rockets;
        }

        public IEnumerable<Rocket> GetSimpleHierarchicalExternalDataCollection()
        { 
            var externalData = new List<Rocket>()
            {
                new Rocket()
                    { 
                        ParentId = null, 
                        RocketId = "2001", 
                        Name = "Falcon", 
                        Title = "Falcon rockets", 
                        Description = "The Falcon rocket family is a set of launch vehicles developed and operated by Space Exploration Technologies (SpaceX), headquartered in Hawthorne, California. They are the first orbital launch vehicles to be entirely designed in the 21st century."
                    },
                new Rocket()
                    { 
                        ParentId = "2001", 
                        RocketId = "2001-1", 
                        Name = "Falcon 1", 
                        Title = "The Falcon 1", 
                        Description = "The Falcon 1 is a small, partially reusable rocket capable of placing several hundred kilograms into low earth orbit. Falcon 1 achieved orbit on its fourth attempt, on 28 September 2008."
                    },
                new Rocket()
                    { 
                        ParentId = "2001", 
                        RocketId = "2001-2", 
                        Name = "Falcon 9", 
                        Title = "The Falcon 9", 
                        Description = "The first version of the Falcon 9, Falcon 9 v1.0, was developed in 2005–2010, and flew five orbital missions in 2010–2013. The second version of the launch system—Falcon 9 v1.1—is the current Falcon 9 in service."
                    },
                
                new Rocket()
                    { 
                        ParentId = null, 
                        RocketId = "2002", 
                        Name = "Titan", 
                        Title = "The Titan Rocket", 
                        Description = "Titan was a family of U.S. expendable rockets used between 1959 and 2005."
                    },
                new Rocket()
                    { 
                        ParentId = null, 
                        RocketId = "2003", 
                        Name = "Saturn", 
                        Title = "Saturn rockets", 
                        Description = "The Saturn family of American rocket boosters was developed by a team of mostly German rocket scientists led by Wernher von Braun to launch heavy payloads to Earth orbit and beyond."
                    },
                new Rocket()
                    { 
                        ParentId = "2003", 
                        RocketId = "2003-1", 
                        Name = "Saturn V", 
                        Title = "The Saturn V", 
                        Description = "The Saturn V was an American human-rated expendable rocket used by NASA's Apollo and Skylab programs between 1966 and 1973."
                    }
            };
            return externalData;
        }
    }
}


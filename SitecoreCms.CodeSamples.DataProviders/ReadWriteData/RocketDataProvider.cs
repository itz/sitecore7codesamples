﻿using System;
using System.Collections.Generic;
using System.Linq;
using Sitecore.Caching;
using Sitecore.Collections;
using Sitecore.Configuration;
using Sitecore.Data;
using Sitecore.Data.DataProviders;
using Sitecore.Data.IDTables;
using Sitecore.Data.Items;
using Sitecore.Data.Managers;
using Sitecore.Data.Templates;
using Sitecore.Diagnostics;
using Sitecore.Reflection;
using SitecoreCms.CodeSamples.Entities;
using Sitecore.Globalization;

namespace SitecoreCms.CodeSamples.DataProviders.ReadWriteData
{
    public class RocketDataProvider : DataProvider
    {
        private readonly string _targetDatabaseName;
        private readonly string _idTablePrefix;
        private readonly ID _rocketTemplateID;
        private readonly ID _rocketRootTemplateID;
        private IExternalDataRepository _externalDataRepository;

        public RocketDataProvider(string targetDatabaseName, string rocketRootTemplateID, string rocketTemplateID, string idTablePrefix)
        {
            Assert.ArgumentNotNullOrEmpty(targetDatabaseName, "targetDatabaseName");
            Assert.ArgumentNotNullOrEmpty(rocketRootTemplateID, "rootTemplateId");
            Assert.ArgumentNotNullOrEmpty(rocketTemplateID, "simpleReadOnlyDataTemplateId");
            Assert.ArgumentNotNullOrEmpty(idTablePrefix, "idTablePrefix");

            _targetDatabaseName = targetDatabaseName;
            _idTablePrefix = idTablePrefix;

            if (!ID.TryParse(rocketRootTemplateID, out _rocketRootTemplateID))
                throw new InvalidOperationException(string.Format("Invalid rocket root template ID {0}", rocketRootTemplateID));

            if (!ID.TryParse(rocketTemplateID, out _rocketTemplateID))
                throw new InvalidOperationException(string.Format("Invalid rocket template ID {0}", rocketTemplateID));

            string path = "codeSamples/rocketRepository";
            var config = Factory.GetConfigNode(path);

            Assert.IsNotNull(config, path);

            _externalDataRepository = Factory.CreateObject<IExternalDataRepository>(config);

            if (_externalDataRepository == null)
                throw new NullReferenceException("The repository of type IExternalDataRepository must not be null");
        }

        public override ItemDefinition GetItemDefinition(ID itemId, CallContext context)
        {            
            Assert.ArgumentNotNull(itemId, "itemID");
            Assert.ArgumentNotNull(context, "context");

            if (context.CurrentResult != null)
                return null;

            // Retrieve the rocket id from Sitecore's IDTable
            var rocketId = GetRocketIdFromIDTable(itemId);

            if (!string.IsNullOrEmpty(rocketId))
            {
                // Retrieve the rocket data from the rockets collection
                var rocket = _externalDataRepository.GetById(rocketId);

                if (rocket != null)
                {
                    context.Abort();

                    // Ensure the rocket item name is valid for the Sitecore content tree
                    var itemName = ItemUtil.ProposeValidItemName(rocket.Name);                                        

                    // Return a Sitecore item definition for the rocket using the rocket template
                    return new ItemDefinition(itemId, itemName, ID.Parse(_rocketTemplateID), ID.Null);
                }
            }

            return null;
        }

        public override VersionUriList GetItemVersions(ItemDefinition item, CallContext context)
        {
            if (CanProcessItem(item.ID))
            { 
                VersionUriList versions = new VersionUriList();      

                // Just a little hack
                versions.Add(Language.Current, Sitecore.Data.Version.First);

                context.Abort();

                return versions;
            }

            return base.GetItemVersions(item, context);
        }

        private bool CanProcessItem(ID id)
        {
            if (IDTable.GetKeys(_idTablePrefix, id).Length > 0)
            {
                return true;
            }
            return false;
        }

        public override FieldList GetItemFields(ItemDefinition itemDefinition, VersionUri version, CallContext context)
        {
            var fields = new FieldList();

            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemDefinition.ID);

            if (idTableEntries.Any())
            {
                if (context.DataManager.DataSource.ItemExists(itemDefinition.ID))
                {
                    ReflectionUtil.CallMethod(typeof(ItemCache), CacheManager.GetItemCache(context.DataManager.Database), "RemoveItem", true, true, new object[] { itemDefinition.ID });
                }

                var template = TemplateManager.GetTemplate(_rocketTemplateID, Factory.GetDatabase(_targetDatabaseName));

                if (template != null)
                {
                    var rocketId = GetRocketIdFromIDTable(itemDefinition.ID);

                    if (!string.IsNullOrEmpty(rocketId))
                    {
                        var rocket = _externalDataRepository.GetById(rocketId);

                        if (rocket != null)
                        {
                            foreach (var field in GetDataFields(template))
                            {
                                fields.Add(field.ID, GetFieldValue(field, rocket));
                            }
                        }
                    }
                }
            }

            return fields;
        }

        protected virtual IEnumerable<TemplateField> GetDataFields(Template template)
        {
            return template.GetFields().Where(ItemUtil.IsDataField);
        }

        private string GetFieldValue(TemplateField field, IRocket rocket)
        {
            string fieldValue = string.Empty;

            switch (field.Name)
            {
                case "Title":
                    fieldValue = rocket.Title;
                    break;
                case "Description":
                    fieldValue = rocket.Description;
                    break;
                default:
                    break;
            }

            return fieldValue;
        }

        public override IDList GetChildIDs(ItemDefinition parentItem, CallContext context)
        {            
            if (CanProcessParent(parentItem.ID))
            {
                var itemIdList = new IDList();

                var rockets = new List<IRocket>();

                if ((parentItem.TemplateID == _rocketRootTemplateID))
                {
                    rockets.AddRange(_externalDataRepository.GetByParentId(null));
                }
                else
                {
                    rockets.AddRange(_externalDataRepository.GetByParentId(parentItem.ID.ToString()));
                }

                foreach (var rocket in rockets)
                {
                    var rocketId = rocket.RocketId;

                    // Retrieve the Sitecore item ID mapped to his rocket
                    IDTableEntry mappedID = IDTable.GetID(_idTablePrefix, rocketId);

                    if (mappedID == null)
                    {
                        // Map this rocket to a Sitecore item ID
                        mappedID = IDTable.GetNewID(_idTablePrefix, rocketId, parentItem.ID);
                    }

                    itemIdList.Add(mappedID.ID);
                }

                context.DataManager.Database.Caches.DataCache.Clear();

                return itemIdList;
            }

            return null;
        }

        public override ID GetParentID(ItemDefinition itemDefinition, CallContext context)
        {
            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemDefinition.ID);

            if (idTableEntries.Any())
            {
                return idTableEntries.First().ParentID;
            }

            return null;
        }

        public override LanguageCollection GetLanguages(CallContext context)
        {
            return null;
        }

        // Private methods
        private string GetRocketIdFromIDTable(ID itemID)
        {
            var idTableEntries = IDTable.GetKeys(_idTablePrefix, itemID);

            if (idTableEntries.Any())
                return idTableEntries[0].Key.ToString();

            return null;
        }

        private bool CanProcessParent(ID id)
        {
            var item = Factory.GetDatabase(_targetDatabaseName).Items[id];

            bool canProcess = false;

            if (item.Paths.IsContentItem && item.TemplateID == _rocketRootTemplateID)
            {
                canProcess = true;
            }

            return canProcess;
        }
    }
}
